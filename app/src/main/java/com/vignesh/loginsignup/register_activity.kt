package com.vignesh.loginsignup

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_main.btn_login
import kotlinx.android.synthetic.main.activity_main.btn_signup
import kotlinx.android.synthetic.main.activity_register.*

class register_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

            val email = findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.et_email) as androidx.appcompat.widget.AppCompatEditText
            val password = findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.et_password) as androidx.appcompat.widget.AppCompatEditText

            btn_signup.setOnClickListener {
            if((et_name.text.toString().isEmpty()) || (email.text.toString().isEmpty()) || (password.text.toString().isEmpty())){
                FancyToast.makeText(this, "Enter all the fields!!", FancyToast.LENGTH_LONG, FancyToast.ERROR,false).show()
            }else if (!(veifyEmail(email))) {
                FancyToast.makeText(this, "Entered email is invalid!!", FancyToast.LENGTH_LONG, FancyToast.ERROR,false).show()
            }
            else if(!(verifyPassword(password))){
                FancyToast.makeText(this, "Entered password is weak!!", FancyToast.LENGTH_LONG, FancyToast.ERROR,false).show()
            }
            else{
                FancyToast.makeText(this, "Registration Success!!", FancyToast.LENGTH_LONG, FancyToast.SUCCESS,false);
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra(Constants.USER_NAME, et_name.text.toString())
                intent.putExtra(Constants.USER_EMAIL, email.text.toString())
                intent.putExtra(Constants.USER_PASSWORD, password.text.toString())
                startActivity(intent)
            }
        }

        btn_login.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun veifyEmail(email: androidx.appcompat.widget.AppCompatEditText): Boolean {
        var emailFlag = false
        val localEmail = email.text.toString()
        if(localEmail.contains("@")){
            emailFlag = true
        }
        return emailFlag
        }

    private fun verifyPassword(password : androidx.appcompat.widget.AppCompatEditText): Boolean{
        var passwordFlag = false
        val localPassword = password.text.toString().trim()
        val pattern : String = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"
        val patternCheck : Boolean = localPassword.matches(pattern.toRegex())
        if (patternCheck && localPassword.length>=8) {
            passwordFlag = true
        }
        return passwordFlag
    }

    }